 <!-- Modal -->
  <div class="modal fade" id="storeModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">New Store</h4>
          <button type="button" class="close" data-dismiss="modal" style="font-size: 25px;color: #ff869a;">&times;</button>
        </div>
        <div class="modal-body">
          <div class="card">
            <form id='new_Sucursal' enctype="multipart/form-data" method="post">
            <div class="card-header">
                <h5>Store registration</h5>
                <div class="card-header-right"><i class="icofont icofont-spinner-alt-5"></i></div>
            </div>
            <div class="card-block tooltip-icon button-list">
                <div class="input-group">
                    <span class="input-group-addon" id="iname"><i class="icofont icofont-user-alt-3"></i></span>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter the store name" title="" data-toggle="tooltip" data-original-title="Enter the store name">
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="iphone"><i class="icofont icofont-phone"></i></span>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter the phone number" title="" data-toggle="tooltip" data-original-title="Enter the phone number">
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="iemail"><i class="icofont icofont-ui-email"></i></span>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" title="" data-toggle="tooltip" data-original-title="Enter email">
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="iaddress"><i class="icofont icofont-location-pin"></i></span>
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter the Address" title="" data-toggle="tooltip" data-original-title="Enter the Address">
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="ilogo"><i class="icofont icofont-image"></i></span>
                    <input type="file" class="form-control" id="logo" name="logo" placeholder="Select a picture" title="" data-toggle="tooltip" data-original-title="Select a picture">
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" data-dismiss="modal" onclick="managestore()">Save</button>
        </div>
            </form>
      </div>
      
    </div>
  </div>

 <!-- Modal -->
 <div class="modal fade" id="ProblemsModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">New Problems</h4>
        <button type="button" class="close" data-dismiss="modal" style="font-size: 25px;color: #ff869a;">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Problem</label>
          <div class="col-sm-10">
            <input type="text" class="form-control"
            placeholder="problem description">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Contractors</label>
          <div class="col-sm-10">
            <select name="select" class="form-control">
              <option value="opt1">Select One Value Only</option>
              <option value="opt2">Computer System</option>
              <option value="opt3">Security System</option>
              <option value="opt4">HVAC</option>
              <option value="opt5">Electrical</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
      </div>
    </div>

  </div>
</div>
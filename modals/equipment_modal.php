 <!-- Modal -->
 <div class="modal fade" id="equipmentModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">New Equipment</h4>
          <button type="button" class="close" data-dismiss="modal" style="font-size: 25px;color: #ff869a;">&times;</button>
        </div>
        <div class="modal-body">
          <form id ="new_equiment" enctype="multipart/form-data" method="post">
                <div class="input-group">
                    <span class="input-group-addon" id="iname"><i class="icofont icofont-tools"></i></span>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter the equiment" title="" data-toggle="tooltip" data-original-title="Enter the equiment">
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="icolor"><i class="icofont icofont-color-picker"></i></span>
                    <input type="text" class="form-control" id="color" name="color" placeholder="Enter the color" title="" data-toggle="tooltip" data-original-title="Enter the color">
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="ibrand"><i class="icofont icofont-search-alt-1"></i></span>
                    <input type="text" class="form-control" id="brand" name="brand" placeholder="Enter the brand" title="" data-toggle="tooltip" data-original-title="Enter the brand">
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="imodel"><i class="icofont icofont-info-circle"></i></span>
                    <input type="text" class="form-control" id="model" name="model" placeholder="Enter the model" title="" data-toggle="tooltip" data-original-title="Enter the model">
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="iyear"><i class="icofont icofont-ui-calendar"></i></span>
                    <input type="text" class="form-control" id="year" name="year" placeholder="Enter the year" title="" data-toggle="tooltip" data-original-title="Enter the year">
                </div>
                <div class="input-group">
                    <span class="input-group-addon" id="iwarranty"><i class="icofont icofont-ui-check"></i></span>
                    <input type="text" class="form-control" id="warranty" name="warranty" placeholder="Enter the warranty" title="" data-toggle="tooltip" data-original-title="Enter the warranty">
                </div>
                <div class="input-group">
                <select id="select" class='form-control'>
                </select>
                </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="managerequiment()">Save</button>
        </div>
      </div>
      
    </div>
  </div>

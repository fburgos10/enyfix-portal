<nav class="pcoded-navbar">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">
        
        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation"></div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="<?php echo $activeHome ?>">
                <a href="home">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">dashboard</i>
                        <b>D</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;top: 10px" data-i18n="nav.dash.main">Dashboard</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.forms">Maintenances</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="<?php echo $activeStore ?>">
                <a href="store">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">store</i>
                        <b>S</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Store</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="<?php echo $activeFixers ?>">
                <a href="fixers">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">build</i>
                        <b>F</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Fixers</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="<?php echo $activeEquipment ?>">
                <a href="equipment">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">kitchen</i>
                        <b>LE</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Location Equipment</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="<?php echo $activePanel ?>">
                <a href="panels">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">tune</i>
                        <b>P</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Panels</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>

        </ul>

        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.forms">Order Maintenances</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="<?php echo $activeOrder ?>">
                <a href="problems">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">local_grocery_store</i>
                        <b>P</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Orders</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="<?php echo $activeContractors ?>">
                <a href="contractors">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">keyboard_tab</i>
                        <b>P</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Contractors</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="<?php echo $activeProblems ?>">
                <a href="problems">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">keyboard_tab</i>
                        <b>P</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Common problems</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            
        </ul>

        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.forms">Equipment Maintenances</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="<?php echo $activeCommon ?>">
                <a href="common-problems">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">new_releases</i>
                        <b>P</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Common problems</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="<?php echo $activeManual ?>">
                <a href="manual">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">menu_book</i>
                        <b>P</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Manuals</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            
        </ul>

        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.forms">Reports</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="<?php echo $activeCommon ?>">
                <a href="#">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">list_alt</i>
                        <b>P</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Report 1</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="<?php echo $activeManual ?>">
                <a href="#">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">list_alt</i>
                        <b>P</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Report 2</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="<?php echo $activeManual ?>">
                <a href="#">
                    <span class="pcoded-micon">
                        <i class="material-icons" style="vertical-align: middle;display: inline-block;">list_alt</i>
                        <b>P</b>
                    </span>
                    <span class="pcoded-mtext" style="vertical-align: middle;display: inline-block;position: absolute;top: 10px;" data-i18n="nav.form-components.main">Report 3</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            
        </ul>
    </div>
</nav>
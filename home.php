<?php include('header/header.php'); ?>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <?php include('header/top-nav.php'); ?>
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <?php $activeHome ="active";
                include('header/left-nav.php'); ?>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">

                                <div class="page-body">
                                  <div class="row">

                                    <!-- order-card start -->
                                    <div class="col-md-6 col-xl-3">
                                        <div class="card bg-c-blue order-card">
                                            <div class="card-block">
                                                <h6 class="m-b-20">Work Orders</h6>
                                                <h2 class="text-right">
                                                    <i class="ti-shopping-cart f-left"></i><span>486</span>
                                                </h2>
                                                <p class="m-b-0">Completed Orders<span class="f-right">52</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xl-3">
                                        <div class="card bg-c-green order-card">
                                            <div class="card-block">
                                                <h6 class="m-b-20">Total users</h6>
                                                <h2 class="text-right">
                                                    <i class="ti-user f-left"></i><span>13</span>
                                                </h2>
                                                <p class="m-b-0">Active users<span class="f-right">9</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xl-3">
                                        <div class="card bg-c-yellow order-card">
                                            <div class="card-block">
                                                <h6 class="m-b-20">Store quantity</h6>
                                                <h2 class="text-right">
                                                    <i class="ti-map-alt f-left"></i><span>3</span>
                                                </h2>
                                                <p class="m-b-0"><span class="f-right"></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xl-3">
                                        <div class="card bg-c-pink order-card">
                                            <div class="card-block">
                                                <h6 class="m-b-20">Total in orders</h6>
                                                <h2 class="text-right"><i class="ti-wallet f-left"></i><span>$9,562</span></h2>
                                                <p class="m-b-0">This Month<span class="f-right">$542</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- order-card end -->

                                    <!-- statustic and process start -->
                                    <div class="col-lg-8 col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Work Order History</h5>
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="fa fa-chevron-left"></i></li>
                                                        <li><i class="fa fa-window-maximize full-card"></i></li>
                                                        <li><i class="fa fa-minus minimize-card"></i></li>
                                                        <li><i class="fa fa-refresh reload-card"></i></li>
                                                        <li><i class="fa fa-times close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <td>Sucursal</td>
                                                            <td>Company</td>
                                                            <td>Creation date</td>
                                                            <td>Due date</td>
                                                            <td>Status</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="orderHistory"></tbody>
                                                </table>
                                            </div>
                                            <!-- <div class="card-block">
                                                <canvas id="Statistics-chart" height="200"></canvas>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Another graph</h5>
                                                <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                                            </div>
                                            <div class="card-block">
                                                <div id="line-example"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- statustic and process end -->
                                    <!-- tabs card start -->
                                    <div class="col-sm-12">
                                        <div class="card tabs-card">
                                            <div class="card-block p-0">
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs md-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#OpenWorkOrders" role="tab"><i class="ti-shopping-cart"></i>Open Work Orders</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#RecentInvoices" role="tab"><i class="ti-receipt"></i>Recent Invoices</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                </ul>
                                                <!-- Tab panes -->
                                                <div class="tab-content card-block">
                                                    <div class="tab-pane active" id="OpenWorkOrders" role="tabpanel">
                                                        <div id="peliculasContainer"></div>
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Image</th>
                                                                        <th>Store</th>
                                                                        <th>Manager</th>
                                                                        <th>Phone</th>
                                                                        <th>Address (click to see on google maps)</th>
                                                                        <th>Status</th>
                                                                        <th>Order ID</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="infoHomeDisplay"></tbody>
                                                            </table>
                                                        </div>
                                                        <div class="text-center">
                                                            <button class="btn btn-outline-primary btn-round btn-sm">Load More</button>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="RecentInvoices" role="tabpanel">

                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <tr>
                                                                    <th>Image</th>
                                                                    <th>Store</th>
                                                                    <th>Assigned to</th>
                                                                    <th>Requested by</th>
                                                                    <th>Status</th>
                                                                    <th>Total</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="image/bill.png" alt="invoice img" class="img-fluid" style="width: 50px;height: 50px;">
                                                                    </td>
                                                                    <td>KFC</td>
                                                                    <td>Company 1</td>
                                                                    <td>Juan Soto</td>
                                                                    <td><span class="label label-success">Completed</span></td>
                                                                    <td>US$ 5,600.00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="image/bill.png" alt="invoice img" class="img-fluid" style="width: 50px;height: 50px;">
                                                                    </td>
                                                                    <td>Burguer King</td>
                                                                    <td>Company 4</td>
                                                                    <td>Pedro Motoa</td>
                                                                    <td><span class="label label-warning">Pending</span></td>
                                                                    <td>US$ 3,200.00</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="text-center">
                                                            <button class="btn btn-outline-primary btn-round btn-sm">Load More</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- tabs card end -->
                                </div>
                            </div>

                            <div id="styleSelector">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/home.js"></script>
<?php include('header/footer.php'); ?>

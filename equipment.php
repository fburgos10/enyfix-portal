<?php include('header/header.php'); ?>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
  <div class="pcoded-overlay-box"></div>
  <div class="pcoded-container navbar-wrapper">

    <?php include('header/top-nav.php'); ?>
    <div class="pcoded-main-container">
      <div class="pcoded-wrapper">
        <?php $activeEquipment ="active";
        include('header/left-nav.php'); 
        include('modals/equipment_modal.php'); 
        include('modals/Details_equiment_modal.php'); 
        ?>
        <div class="pcoded-content">
          <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
              <div class="page-wrapper">
               <!-- Page-header start -->
               <div class="page-header card">
                <div class="card-block">
                  <h5 class="m-b-10">Location Equipment</h5>
                  <p class="text-muted m-b-10" id = "null">lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                  <ul class="breadcrumb-title b-t-default p-t-10">
                    <li class="breadcrumb-item">
                      <a href="home"> <i class="fa fa-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Enyfix</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Equipment info</a>
                    </li>
                  </ul>
                </div>
              </div>
              <!-- Page-header end -->

              <!-- Page-body start -->
              <div class="page-body">
                <!-- Basic table card start -->
                <div class="card">
                  <div class="card-header">
                    <h5>All Equipment</h5>
                    <div class="card-header-right">
                      <ul class="list-unstyled card-option" style="float: left;">
                       <li><i class="fa fa-chevron-left"></i></li>
                       <li><i class="fa fa-window-maximize full-card"></i></li>
                       <li><i class="fa fa-minus minimize-card"></i></li>
                       <li><i class="fa fa-refresh reload-card"></i></li>
                       <li><i class="fa fa-times close-card" style="display: none"></i></li>
                     </ul>
                     <button class="btn btn-primary btn-round" style="float: right;margin-top: -7px;" data-toggle="modal" data-target="#equipmentModal" onclick ="action = 0">Create new Equip</button>
                   </div>

                 </div>
                 <div class="card-block table-border-style">
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Images</th>
                          <th>ID</th>
                          <th>Equiment</th>
                          <th>Color</th>
                          <th>Brand</th>
                          <th>Model</th>
                          <th>Year</th>
                          <th>Assigned Store</th>
                          <th align="center">Serial Number</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id='loadequiment'>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- Basic table card end -->

            </div>
            <!-- Page-body end -->
          </div>
        </div>
        <!-- Main-body end -->

        <div id="styleSelector">

        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script src = 'js/equiment.js'
<?php include('header/footer.php'); ?>
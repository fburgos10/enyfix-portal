const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');

const app = ('app');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('/'));


app.get('/', (req, res) =>{
   
   res.setHeader('Content-tpe', 'text/html');
   res.SendFile('login.php');
})

app.listen(3001, () =>{
  console.log('Servidor Iniciado...');
});


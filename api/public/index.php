<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../src/config/db.php';

//Display errores
$config = ["settings" => [
  "displayErrorDetails" => true
]];

$app = new Slim\App($config);

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->get('/welcome', function ($request, $response, $args) {
    
   $message = [
            'Notice ' => 'Bienvenido a Moviwash',
            'Tiempo de carga' => time(),
            'Fecha de actulizacion' => date('Y-m-d'),
        ];

		$payload = json_encode($message);
		$response->getBody()->write($payload);
		return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
});

// Customer Routes
require '../src/routes/user.php'; //Api de cliente info
require '../src/routes/login.php';
require '../src/routes/direcciones.php';
require '../src/routes/infoCart.php';
require '../src/routes/factura.php';
require '../src/routes/ordenes.php';
require '../src/routes/slideHome.php';
require '../src/routes/memos.php';
require '../src/routes/negocio.php';
require '../src/routes/inventory.php';
require '../src/routes/orders.php';
require '../src/routes/foto.php';
require '../src/routes/usuario.php';
require '../src/routes/itemOrders.php';
require '../src/routes/detalleOrder.php';
require '../src/routes/company.php';
require '../src/routes/sucursales.php';
//require '../src/routes/locations.php';
$app->run();

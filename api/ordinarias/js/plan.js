$(document).ready(function(){

$('#plan').on('change', function() {
  var thk = this.value;

    var html ="";
    if (thk == 'plan_FnEVApCmculytF') {
    	
        html =`<h2 >Monthly Plan</h2>
              <h3>$75<sup>.00</sup> <span>/ each month</span></h3>
              <ul>
                <li>Search all 50 states <i class="fa fa-check icon-success"></i></li>
                <li>Daily email alerts <i class="fa fa-check icon-success"></i></li>
                <li>Access to all resources <i class="fa fa-check icon-success"></i></li>
                <li>Add users for + $25.00/month <i class="fa fa-check icon-success"></i></li>
              </ul>`;

    }else if(thk == 'plan_FoPZgRii0RIhqs'){
        html =`<h2 style="background-color:#FFDA47;">Yearly Plan</h2>
							<h3 style="background-color:#FFBB00;">$750<sup>.00</sup> <span>/ one time</span></h3>
							<ul>
								<li>Search all 50 states <i class="fa fa-check icon-success"></i></li>
								<li>Daily email alerts <i class="fa fa-check icon-success"></i></li>
								<li>Access to all resources <i class="fa fa-check icon-success"></i></li>
								<li>Add users for + $125.00/year <i class="fa fa-check icon-success"></i></li>
								<li>One low price <i class="fa fa-check icon-success"></i></li>
							</ul>`;
    }else{
       html =`<h2 style="background-color:#61b764;">Quarterly Plan</h2>
							<h3 style="background-color:#45A049;">$200<sup>.00</sup> <span>/ every 3 months</span></h3>
							<ul>
								<li>Search all 50 states <i class="fa fa-check icon-success"></i></li>
								<li>Daily email alerts <i class="fa fa-check icon-success"></i></li>
								<li>Access to all resources <i class="fa fa-check icon-success"></i></li>
								<li>Add users for + $50.00 <i class="fa fa-check icon-success"></i></li>
							</ul>`;
    }

    $('.general').html(html);
});

});
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
header("Access-Control-Allow-Origin:*");
require_once('vendor/autoload.php');
require_once('config/db.php');
require_once('lib/pdo_db.php');
require_once('models/saveID.php');
require_once('models/saveOrden.php');

// Sanitize POST Array
$POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

try
{

  $requestedBy = $POST['requestedBy']; // ID de quien requiere la tarea.
  $assignedComapny = $POST['assignedComapny']; // ID de compa�ia asignada.
  $dateAdded = $POST['dateAdded']; // Fecha dde creacion.
  $dueDate = $POST['dueDate']; // Fecha de vencimiento.
  $conversations = $POST['conversations']; // Nota
  $priority = $POST['priority']; // Prioridad de la orden.
  $id_user = $POST['id_user']; // ID del usuario.
  $id_sucursal = $POST['id_sucursal']; // ID de la sucursal.
  $fecha = date('Y-m-d');

// Crear la orden
  $OrdenData = [
    'id' => $id_user,
    'assignedComapny' => $assignedComapny,
    'dateAdded' => $dateAdded,
    'dueDate' => $dueDate,
    'conversations' => $conversations,
    'priority' => $priority,
    'id_sucursal' => $id_sucursal
  ];
  $orden = new Orden();
  $orden->addOrden($OrdenData);
  
// Extraer ID de la orden insertada
  $jsonDesc = new Orden();
  $resultado = $jsonDesc->getOrden();
  foreach ($resultado as $row) {
    $id_orden = $row->id;
  }

// Actulizar el detalle de la orden
  $detalleData = [
    'id_orden' => $id_orden,
    'id_user' => $id_user
  ];
  $detalle = new ActulizarDetalle();
  $detalle->updateDetalle($detalleData);
  echo "1"; // Insercion exitosa.
}
catch(Exception $e){
  error_log("Error :". $e->getMessage());
  echo "2"; // Display errors
}
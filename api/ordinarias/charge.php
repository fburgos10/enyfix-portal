<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
header("Access-Control-Allow-Origin:*");
require_once('vendor/autoload.php');
require_once('config/db.php');
require_once('lib/pdo_db.php');
require_once('models/notas.php');
require_once('models/saveDetalle.php');

// Sanitize POST Array
$POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

try
{

  $contractors = $POST['contractors']; //ID contractors
  $workType = $POST['workType']; //ID workType
  $appliance = $POST['appliance']; //ID appliance
  $Ulocation = $POST['Ulocation']; //ID location
  $notas = $POST['note']; // Nota
  $id_user = $POST['id_user']; // ID del usuario.
  $fecha = date('Y-m-d');

// Crear nota de la tarea
  $notaData = [
    'notas' => $notas,
    'fecha' => $fecha
  ];
  $nota = new Notas();
  $nota->addNota($notaData);
  
  // Extraer ID de la nota
  $jsonDesc = new Notas();
  $resultado = $jsonDesc->getNota();
  foreach ($resultado as $row) {
    $id_nota = $row->id;
  }

  // Insertar el detalle dela tarea
  $detalleData = [
    'contractors' => $contractors,
    'workType' => $workType,
    'appliance' => $appliance,
    'Ulocation' => $Ulocation,
    'id_note' => $id_nota,
    'id_user' => $id_user
  ];
  $detalle = new saveDetalle();
  $detalle->addDetalle($detalleData);
  echo "1"; // Insercion exitosa.
}
catch(Exception $e){
  error_log("Error :". $e->getMessage());
  echo "2"; // Display errors
}
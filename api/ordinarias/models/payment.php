<?php
  class Payment {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function addpayment($data) {
      // Prepare Query
      $this->db->query('INSERT INTO payments_payment (username, transaction_amount, package_subscribed, description, transaction_datetime,trasaction_code, transaction_id) VALUES(:username, :transaction_amount, :package_subscribed, :description, :transaction_datetime, :transaction_code, :transaction_id)');

      // Bind Values
      $this->db->bind(':username', $data['username']);
      $this->db->bind(':transaction_amount', $data['transaction_amount']);
      $this->db->bind(':package_subscribed', $data['package_subscribed']);
      $this->db->bind(':description', $data['description']);
      $this->db->bind(':transaction_datetime', $data['transaction_datetime']);
      $this->db->bind(':transaction_code', $data['transaction_code']);
      $this->db->bind(':transaction_id', $data['transaction_id']);

      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    public function updateUser($data) {
      // Prepare Query
       
      $this->db->query('UPDATE user SET last_transaction_id = :transaction_id, user_type ="Registered User" WHERE username= :username ');

      // Bind Values
      $this->db->bind(':username', $data['username']);
      $this->db->bind(':transaction_id', $data['transaction_id']);

      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    public function getPayment() {
      $this->db->query('SELECT * FROM payments_payment ORDER BY transaction_id DESC');

      $results = $this->db->resultset();

      return $results;
    }
  }
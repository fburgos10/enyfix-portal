<?php
  class Orden {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function addOrden($data) {
      // Prepare Query
      $this->db->query('INSERT INTO ordenes (costo_total, id_pago, id_user, id_direccion_r, id_direccion_e, id_tanda_r, id_tanda_e, fecha_recogida, fecha_entrega) VALUES(:costo_total, :id_pago, :id_user, :id_direccion_r, :id_direccion_e, :id_tanda_r, :id_tanda_e, :fecha_recogida, :fecha_entrega)');

      // Bind Values
      $this->db->bind(':costo_total', $data['costo_total']);
      $this->db->bind(':id_pago', $data['id_pago']);
      $this->db->bind(':id_user', $data['id_user']);
      $this->db->bind(':id_direccion_r', $data['id_direccion_r']);
      $this->db->bind(':id_direccion_e', $data['id_direccion_e']);
      $this->db->bind(':id_tanda_r', $data['id_tanda_r']);
      $this->db->bind(':id_tanda_e', $data['id_tanda_e']);
      $this->db->bind(':fecha_recogida', $data['fecha_recogida']);
      $this->db->bind(':fecha_entrega', $data['fecha_entrega']);

      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    public function getOrden($data) {
      $this->db->query("SELECT id FROM ordenes WHERE id = (SELECT MAX(id) FROM ordenes WHERE id_user = :id )");
      
      $this->db->bind(':id', $data['id']);

      $results = $this->db->resultset();
      return $results;
      print_r($results);
    }
  }
<?php
  class Cart {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function getOCart($data) {
      $this->db->query("UPDATE detalle_venta SET status = 2, idOrden = :id WHERE idCliente = :id_user AND status = 1");
      // Bind Values
      $this->db->bind(':id', $data['id']);
      $this->db->bind(':id_user', $data['id_user']);

      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }
  }
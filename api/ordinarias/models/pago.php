<?php
  class Pagos {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function addPago($data) {
      // Prepare Query
      $this->db->query('INSERT INTO pagos (monto, transaction, moneda, id_metodo) VALUES(:monto, :transaction, :moneda, :id_metodo)');

      // Bind Values
      $this->db->bind(':monto', $data['monto']);
      $this->db->bind(':transaction', $data['transaction']);
      $this->db->bind(':moneda', $data['moneda']);
      $this->db->bind(':id_metodo', $data['id_metodo']);

      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    public function getPago() {
      $this->db->query('SELECT * FROM pagos ORDER BY id DESC');

      $results = $this->db->resultset();

      return $results;
    }
  }
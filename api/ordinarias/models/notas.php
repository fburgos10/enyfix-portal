<?php
  class Notas {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function addNota($data) {
      // Prepare Query
      $this->db->query('INSERT INTO notas (notas, fecha) VALUES (:notas, :fecha)');
      // Bind Values
      $this->db->bind(':notas', $data['notas']);
      $this->db->bind(':fecha', $data['fecha']);
      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    public function getNota() {
      $this->db->query("SELECT MAX(id) AS id FROM notas");
      $results = $this->db->resultset();
      return $results;
      //print_r($results);
    }
  }
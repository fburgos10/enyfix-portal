<?php
  class ActulizarDetalle {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function updateDetalle($data) {
      // Prepare Query
      $this->db->query('UPDATE detalle_orders SET id_order = :id_orden, estado = :estado WHERE estado = 1 AND id_user = :id_user');

      // Bind Values
      $this->db->bind(':id_orden', $data['id_orden']);
      $this->db->bind(':estado', 2);
      $this->db->bind(':id_user', $data['id_user']);
      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

  }
<?php
  class JsonOrder {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function getJson($data) {
      $this->db->query("SELECT a.id, a.articulo AS nombre, a.tipo , a.id AS id_art, a.precioPlanchado AS planc, a.precioLavadoPlanchado AS lav_planc, d.cantidad as cant, d.precioUnitario as price FROM detalle_venta d INNER JOIN articulos a ON (a.id = d.`idArticulo`) WHERE d.idOrden = :id");
      
      $this->db->bind(':id', $data['id']);

      $results = $this->db->resultset();
      return $results;
      print_r($results);
    }
  }
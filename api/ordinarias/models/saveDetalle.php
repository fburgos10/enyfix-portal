<?php
  class saveDetalle {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function addDetalle($data) {
      // Prepare Query
      $this->db->query('INSERT INTO detalle_orders (contractors, problems, location, equipo, id_note, id_user)
        VALUES (:contractors, :workType, :Ulocation, :appliance, :id_note, :id_user)');
      // Bind Values
      $this->db->bind(':contractors', $data['contractors']);
      $this->db->bind(':workType', $data['workType']);
      $this->db->bind(':Ulocation', $data['Ulocation']);
      $this->db->bind(':appliance', $data['appliance']);
      $this->db->bind(':id_note', $data['id_note']);
      $this->db->bind(':id_user', $data['id_user']);
      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    public function getDetalle() {
      $this->db->query("SELECT MAX(id) AS id FROM notas");
      $results = $this->db->resultset();
      return $results;
      //print_r($results);
    }
  }
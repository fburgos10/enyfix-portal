<?php
  class Orden {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function addOrden($data) {
      // Prepare Query
      $this->db->query('INSERT INTO orders (id_sucursal, id_user, id_compania, fecha_creacion,fecha_vencimiento,prioridad,conversations)
        VALUES (:id_sucursal, :id, :assignedComapny, :dateAdded, :dueDate, :priority, :conversations)');

      // Bind Values
      $this->db->bind(':id_sucursal', $data['id_sucursal']);
      $this->db->bind(':id', $data['id']);
      $this->db->bind(':assignedComapny', $data['assignedComapny']);
      $this->db->bind(':dateAdded', $data['dateAdded']);
      $this->db->bind(':dueDate', $data['dueDate']);
      $this->db->bind(':conversations', $data['conversations']);
      $this->db->bind(':priority', $data['priority']);
      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    public function getOrden() {
      $this->db->query("SELECT MAX(id) AS id FROM orders WHERE estatus = 1");
      $results = $this->db->resultset();
      return $results;
      //print_r($results);
    }
  }
<?php
  class Tarea {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function addTarea($data) {
      // Prepare Query
      $this->db->query('INSERT INTO tarea (task_id, cliente, tipo, descripcion, direccion, fecha, botones,lat,longi,fecha_r,hora_r,hora_e,fecha_e) 
        VALUES (:task_id, :cliente, :tipo, :descripcion, :adres, :fecha, :botones, :lat, :long, :fecha_r, :hora_r, :hora_e, :fecha_e)');

      // Bind Values
      $this->db->bind(':task_id', $data['task_id']);
      $this->db->bind(':cliente', $data['cliente']);
      $this->db->bind(':tipo', $data['tipo']);
      $this->db->bind(':descripcion', $data['descripcion']);
      $this->db->bind(':adres', $data['adres']);
      $this->db->bind(':fecha', $data['fecha']);
      $this->db->bind(':botones', $data['botones']);
      $this->db->bind(':lat', $data['lat']);
      $this->db->bind(':long', $data['long']);
      $this->db->bind(':fecha_r', $data['fecha_r']);
      $this->db->bind(':hora_r', $data['hora_r']);
      $this->db->bind(':fecha_e', $data['fecha_e']);
      $this->db->bind(':hora_e', $data['hora_e']);

      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    public function getTarea($data) {
      $this->db->query("SELECT task_id FROM tarea WHERE task_id = (SELECT MAX(task_id) FROM ordenes WHERE tipo = :task_id )");
      
      $this->db->bind(':task_id', $data['task_id']);

      $results = $this->db->resultset();
      return $results;
      //print_r($results);
    }
  }
<?php

// API Precio de  Lavado y Planchado 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
header("Access-Control-Allow-Origin:*");
require_once ("config/db.php");
require_once ("config/conexion.php");

$sql = "SELECT * FROM articulos";
$result = $con->query($sql);
$return_arr = array();
$contador = 0;
while ($result_row = $result->fetch_object()) {

	$LavadoPlanchado[$contador] = $result_row->precioLavadoPlanchado;
	$contador++;
}

list($LavadoPlanchado1, $LavadoPlanchado2, $LavadoPlanchado3, $LavadoPlanchado4, $LavadoPlanchado5, $LavadoPlanchado6, $LavadoPlanchado7, $LavadoPlanchado8, $LavadoPlanchado9, $LavadoPlanchado10, $LavadoPlanchado11, $LavadoPlanchado12, $LavadoPlanchado13, $LavadoPlanchado14, $LavadoPlanchado15) = $LavadoPlanchado;

// Precio de Lavado y LavadoPlanchado Hombres
$row_array['precioCamisa'] = $precioCamisa = $LavadoPlanchado1;
$row_array['precioPantalon'] = $precioPantalon = $LavadoPlanchado2;
$row_array['preciotSaco'] = $preciotSaco = $LavadoPlanchado3;
$row_array['precioTraje'] = $precioTraje = $LavadoPlanchado7;
$row_array['precioCorbata'] = $precioCorbata = $LavadoPlanchado14;
$row_array['precioChacabana'] = $precioChacabana = $LavadoPlanchado15;
$row_array['precioSweater'] = $precioSweater = $LavadoPlanchado4;

// Precio de Lavado y LavadoPlanchado Mujeres
$row_array['precioFalda'] = $precioFalda = $LavadoPlanchado5;
$row_array['precioFaldaLarga'] = $precioFaldaLarga =$LavadoPlanchado6;
$row_array['precioVestidoCorto'] = $precioVestidoCorto =$LavadoPlanchado8;
$row_array['precioVestidoLargo'] = $precioVestidoLargo =$LavadoPlanchado9;
$row_array['precioChaquetaMujer'] = $precioChaquetaMujer =$LavadoPlanchado10;
$row_array['precioBlusaMujer'] = $precioBlusaMujer =$LavadoPlanchado11;
$row_array['precioPantalonMujer'] = $precioPantalonMujer =$LavadoPlanchado12;
$row_array['precioPiezasMujer'] = $precioPiezasMujer =$LavadoPlanchado13;

array_push($return_arr,$row_array);
//echo '<pre>', json_encode($return_arr, JSON_PRETTY_PRINT), '</pre>';
echo json_encode($return_arr, JSON_PRETTY_PRINT); 
$con->close();

?>
<?php
  header("Access-Control-Allow-Origin:*");
  require_once ("config/db.php");
  require_once ("config/conexion.php");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set('America/Santo_Domingo');
// Variables.
$id = $_POST['id']; // ID del cliente
$idService = $_POST['idService']; // Tipo de servicio(Lavado o planchado).

// Ropa de Hombres
$inputCamisa = $_POST['inputCamisaPlus']; // Cantidad de camisas.
$inputPantalon = $_POST['inputPantalonPlus']; // Cantidad de Pantalones.
$inputSaco = $_POST['inputSacoPlus']; // Cantidad de Saco.
$inputTraje = $_POST['inputTrajePlus']; // Cantidad de Traje.
$inputCorbata = $_POST['inputCorbataPlus']; // Cantidad de Corbata.
$inputChacabana = $_POST['inputChacabanaPlus']; // Cantidad de Chacabana.

// Ropa de Mujeres
$inputFalda = $_POST['inputFaldaPlus']; // Cantidad de Falda.
$inputFaldaLarga = $_POST['inputFaldaLargaPlus']; // Cantidad Falda Larga.
$inputVestidoCorto = $_POST['inputVestidoCortoPlus']; // Cantidad Vestido Corto.
$inputVestidoLargo = $_POST['inputVestidoLargoPlus']; // Cantidad Vestido Largo.
$inputChaquetaMujer = $_POST['inputChaquetaMujerPlus']; // Cantidad Chaqueta de Mujer.
$inputBlusaMujer = $_POST['inputBlusaMujerPlus']; // Cantidad Blusa de Mujer.
$inputPantalonMujer = $_POST['inputPantalonMujerPlus']; // Cantidad Pantalon de Mujer.
$inputPiezasMujer = $_POST['inputPiezasMujerPlus']; // Cantidad Traje 2 piezas de Mujer.

$fecha = date('Y-m-d');




if($idService == 1){ 

// Consulta de precios 
$sql = "SELECT * FROM articulos";
$result = $con->query($sql);
$contador = 0;
while ($result_row = $result->fetch_object()) {

  $var[$contador] = $result_row->precioLavadoPlanchado;
  $contador++;
}

list($var1, $var2, $var3, $var4, $var5, $var6, $var7, $var8, 
  $var9, $var10, $var11, $var12, $var13, $var14, $var15) = $var;

// Precio de Lavado y Planchado Hombres
 $precioCamisa = $var1;
 $precioPantalon = $var2;
 $preciotSaco = $var3;
 $precioTraje = $var7;
 $precioCorbata = $var14;
 $precioChacabana = $var15;
 $precioSweater = $var4;

// Precio de Lavado y Planchado Mujeres
$precioFalda = $var5;
$precioFaldaLarga =$var6;
$precioVestidoCorto =$var8;
$precioVestidoLargo =$var9;
$precioChaquetaMujer =$var10;
$precioBlusaMujer =$var11;
$precioPantalonMujer =$var12;
$precioPiezasMujer =$var13;


/*// Precio de Lavado y Planchado Hombres
$precioCamisa = 125; // Precio de la camisa en Lavado y Planchado
$precioPantalon = 125; // Precio del Pantalon en Lavado y Planchado
$preciotSaco = 125; // Precio del Saco en Lavado y Planchado
$precioTraje = 335; // Precio del Traje en Lavado y Planchado
$precioCorbata = 100; // Precio del Corbata en Lavado y Planchado
$precioChacabana = 160; // Precio del Chacabana en Lavado y Planchado

// Precio de Lavado y Planchado Mujeres
$precioFalda =130;
$precioFaldaLarga =130;
$precioVestidoCorto =315;
$precioVestidoLargo =315;
$precioChaquetaMujer =190;
$precioBlusaMujer =130;
$precioPantalonMujer =130;
$precioPiezasMujer =340;*/

}else{

$sql = "SELECT * FROM articulos";
$result = $con->query($sql);
$contador = 0;
while ($result_row = $result->fetch_object()) {

  $var[$contador] = $result_row->precioPlanchado;
  $contador++;
}

list($var1, $var2, $var3, $var4, $var5, $var6, $var7, $var8, 
  $var9, $var10, $var11, $var12, $var13, $var14, $var15) = $var;

// Precio de  Planchado Hombres
 $precioCamisa = $var1;
 $precioPantalon = $var2;
 $preciotSaco = $var3;
 $precioTraje = $var7;
 $precioCorbata = $var14;
 $precioChacabana = $var15;
 $precioSweater = $var4;

// Precio de  Planchado Mujeres
$precioFalda = $var5;
$precioFaldaLarga =$var6;
$precioVestidoCorto =$var8;
$precioVestidoLargo =$var9;
$precioChaquetaMujer =$var10;
$precioBlusaMujer =$var11;
$precioPantalonMujer =$var12;
$precioPiezasMujer =$var13;

/*// Precio de  Planchado Hombres
$precioCamisa = 105; // Precio de la camisa en solo planchado
$precioPantalon = 105; // Precio del Pantalon en solo planchado
$preciotSaco = 105; // Precio del Saco en solo planchado
$precioTraje = 335; // Precio del Traje en solo planchado
$precioCorbata = 100; // Precio del Corbata en solo planchado
$precioChacabana = 160; // Precio del Chacabana en solo planchado

// Precio de  Planchado Mujeres
$precioFalda =110;
$precioFaldaLarga =110;
$precioVestidoCorto =240;
$precioVestidoLargo =235;
$precioChaquetaMujer =165;
$precioBlusaMujer =110;
$precioPantalonMujer =110;
$precioPiezasMujer =270;*/

}


// Totales ropa de Hombres
$precioTotalCamisa = $inputCamisa * $precioCamisa; // Precio total de cantidad de camisas por el precio
$precioTotalPantalon = $inputPantalon * $precioPantalon; // Precio total de cantidad de pantalones por el precio
$precioTotalSaco = $inputSaco * $preciotSaco; // Precio total de cantidad de saco por el precio
$precioTotalTraje = $inputTraje * $precioTraje; // Precio total de cantidad de traje por el precio
$precioTotalCorbata = $inputCorbata * $precioCorbata; // Precio total de cantidad de Corbata por el precio
$precioTotalChacabana = $inputChacabana * $precioChacabana; // Precio total de cantidad de Corbata por el precio

// Totales ropa de mujer
$precioTotalFalda = $inputFalda * $precioFalda;
$precioTotalFaldaLarga = $inputFaldaLarga * $precioFaldaLarga;
$precioTotalVestidoCorto = $inputVestidoCorto * $precioVestidoCorto;
$precioTotalVestidoLargo = $inputVestidoLargo * $precioVestidoLargo;
$precioTotalChaquetaMujer = $inputChaquetaMujer * $precioChaquetaMujer;
$precioTotalBlusaMujer = $inputBlusaMujer * $precioBlusaMujer;
$precioTotalPantalonMujer = $inputPantalonMujer * $precioPantalonMujer;
$precioTotalPiezasMujer = $inputPiezasMujer * $precioPiezasMujer;

// Array con toda la info.            //mujeres
$arrayArticulos = array(1,2,3,7,14,15,5,6,8,9,10,11,12,13); //Listar las variables con los nombres de articulos
$arrayCantidad = array($inputCamisa, $inputPantalon, $inputSaco, $inputTraje, $inputCorbata, $inputChacabana, $inputFalda, $inputFaldaLarga, $inputVestidoCorto, $inputVestidoLargo, $inputChaquetaMujer, $inputBlusaMujer, $inputPantalonMujer,$inputPiezasMujer);

$arrayPrecios = array($precioCamisa, $precioPantalon, $preciotSaco, $precioTraje, $precioCorbata, $precioChacabana, $precioFalda, $precioFaldaLarga, $precioVestidoCorto, $precioVestidoLargo, $precioChaquetaMujer, $precioBlusaMujer, $precioPantalonMujer,$precioPiezasMujer);

$arrayTotal = array($precioTotalCamisa, $precioTotalPantalon, $precioTotalSaco, $precioTotalTraje, $precioTotalCorbata, $precioTotalChacabana, $precioTotalFalda,$precioTotalFaldaLarga, $precioTotalVestidoCorto, $precioTotalVestidoLargo, $precioTotalChaquetaMujer, $precioTotalBlusaMujer, $precioTotalPantalonMujer,$precioTotalPiezasMujer);


foreach($arrayArticulos as $llave=>$articulos){

  $array_final[$articulos] = [
    'cantidad' => $arrayCantidad[$llave], 
    'precio' => $arrayPrecios[$llave],
    'total' => $arrayTotal[$llave]
  ];
}

foreach($array_final as $articulos=>$detalle){
  $sql="INSERT INTO detalle_venta (idCliente, idArticulo, cantidad, precioUnitario, precioTotal,fecha,tipoServicio) VALUES ('$id','$articulos','$detalle[cantidad]','$detalle[precio]','$detalle[total]','$fecha','$idService')";
  $result= $con->query($sql);
}
?>
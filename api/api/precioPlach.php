<?php

// API Precio de Lavado y Planchado 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
header("Access-Control-Allow-Origin:*");
require_once ("config/db.php");
require_once ("config/conexion.php");

$sql = "SELECT * FROM articulos";
$result = $con->query($sql);
$return_arr = array();
$contador = 0;
while ($result_row = $result->fetch_object()) {

	$planchado[$contador] = $result_row->precioPlanchado;
	$contador++;
}

list($planchado1, $planchado2, $planchado3, $planchado4, $planchado5, $planchado6, $planchado7, $planchado8, $planchado9, $planchado10, $planchado11, $planchado12, $planchado13, $planchado14, $planchado15) = $planchado;

// Precio de Lavado y Planchado Hombres
$row_array['precioCamisa'] = $precioCamisa = $planchado1;
$row_array['precioPantalon'] = $precioPantalon = $planchado2;
$row_array['preciotSaco'] = $preciotSaco = $planchado3;
$row_array['precioTraje'] = $precioTraje = $planchado7;
$row_array['precioCorbata'] = $precioCorbata = $planchado14;
$row_array['precioChacabana'] = $precioChacabana = $planchado15;
$row_array['precioSweater'] = $precioSweater = $planchado4;

// Precio de Lavado y Planchado Mujeres
$row_array['precioFalda'] = $precioFalda = $planchado5;
$row_array['precioFaldaLarga'] = $precioFaldaLarga =$planchado6;
$row_array['precioVestidoCorto'] = $precioVestidoCorto =$planchado8;
$row_array['precioVestidoLargo'] = $precioVestidoLargo =$planchado9;
$row_array['precioChaquetaMujer'] = $precioChaquetaMujer =$planchado10;
$row_array['precioBlusaMujer'] = $precioBlusaMujer =$planchado11;
$row_array['precioPantalonMujer'] = $precioPantalonMujer =$planchado12;
$row_array['precioPiezasMujer'] = $precioPiezasMujer =$planchado13;

array_push($return_arr,$row_array);
//echo '<pre>', json_encode($return_arr, JSON_PRETTY_PRINT), '</pre>';
echo json_encode($return_arr, JSON_PRETTY_PRINT); 
$con->close();

?>
<?php
// Consultar todos los Slide Home
$app->get('/api/slidehome', function ($request, $response, $args) {

    $sql = "SELECT * FROM slideHome";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Anadir Slide Home
/*$app->post('/api/slidehome/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $usuario = $request->getParam('usuario');
    $nombre = $request->getParam('nombre');
    $apellido = $request->getParam('apellido');
    $direccion = $request->getParam('direccion');
    $cedula = $request->getParam('cedula');
    $telefono = $request->getParam('telefono');
    $email = $request->getParam('email');
    $foto = $request->getParam('foto');
    $pass = $request->getParam('pass');
    $cedula = $request->getParam('cedula');
    $fecha =  date("Y-m-d H:i:s");
    $adres = utf8_decode($direccion);
    $hash = password_hash($pass, PASSWORD_DEFAULT);

    $sql ="INSERT INTO slideHome (nombre, apellido, direccion, cedula, telefono, email, foto, pass, fecha_creacion,usuario) VALUES
    ('$nombre','$apellido','$adres','$cedula','$telefono','$email','$foto','$hash','$fecha','$usuario')";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Cliente registrado con exitos',
          'Company' => 'Moviwash',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});*/


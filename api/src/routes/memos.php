<?php
// Consultar todos los clientes
$app->get('/api/memos', function ($request, $response, $args) {

    $sql = "SELECT * FROM notas";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/memos/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT * FROM notas WHERE id_task = '$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->delete('/api/memos/delete/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "DELETE FROM notas WHERE id_task = ".$id;

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Memo successfully removed',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->post('/api/memos/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $nota = $request->getParam('nota');
    $id = $request->getParam('id');
    $fecha =  date("Y-m-d H:i:s a"); 
    
    $sql ="INSERT INTO notas (notas, fecha, id_task)
           VALUES ('$nota','$fecha','$id')";

    try{

        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Memo successfully registered',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});




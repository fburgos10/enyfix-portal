<?php
// Consultar todos los clientes
$app->get('/api/pagos', function ($request, $response, $args) {

    $sql = "SELECT * FROM metodo_pago";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


// Añadir un nuevo cliente
$app->post('/api/pagos/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $numero = $request->getParam('numero');
    $mes = $request->getParam('mes');
    $ano = $request->getParam('ano');
    $cvv =  $request->getParam('cvv');
    $id_user =  $request->getParam('id_user');
    $tipo_tarjeta =  $request->getParam('tipo_tarjeta');
    $fecha =  date("Y-m-d H:i:s"); 
    
    $tipos = substr($numero,0,1);
    
    if($tipos == 4){
        $tipo ="VISA";
    }else if($tipos == 3){
       $tipo ="AMEX"; 
    }else{
       $tipo ="MasterCard"; 
    }

    $sql ="INSERT INTO metodo_pago 
    ( numero,mes,ano,cvv,moneda,tipo,id_user, tarjeta)
           VALUES
    ('$numero','$mes','$ano','$cvv','DOP','$tipo','$id_user','$tipo_tarjeta')";

    try{
        // Get D
        
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Metodo de pago registrado con exitos',
          'Company' => 'Moviwash',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

//Cambiar contraseña del cliente
$app->delete('/api/pagos/delete/{id}', function ($request, $response, $args) {

    //$id = $request->getAttribute('id');
    $id = $args['id'];
    $sql = "DELETE FROM metodo_pago WHERE id = ".$id;

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Pago Eliminado con exitos',
          'Company' => 'Moviwash',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


<?php

$app->get('/api/contractors/{id}', function ($request, $response, $args) {

    $id = $args['id']; // ID negocio
    $sql = "SELECT  * FROM contractors WHERE negocio = '$id'";

    try{

        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/problems/{id}', function ($request, $response, $args) {

    $id = $args['id']; // ID del contractors
    $sql = "SELECT * FROM problems WHERE id_contractors = '$id'";

    try{

        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/equipo/{id}', function ($request, $response, $args) {

    $id = $args['id']; // ID del contractors
    $sql = "SELECT * FROM equipo WHERE categoria = '$id'";

    try{

        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/location/{id}', function ($request, $response, $args) {

    $id = $args['id']; // ID del equipo
    $sql = "SELECT l.id,l.location FROM equipo e
            INNER JOIN location l ON (l.id = e.ubicacion)
            WHERE e.id = '$id'";

    try{

        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});
<?php

$app->get('/api/detalleOrder/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT d.id, c.contractors, p.problem, l.location, e.equipo, n.notas,estado
            FROM detalle_orders d
            INNER JOIN contractors c ON (c.id = d.contractors)
            INNER JOIN problems p ON (p.id = d.problems)
            INNER JOIN location l ON (l.id = d.location)
            INNER JOIN equipo e ON (e.id = d.equipo)
            INNER JOIN notas n ON (n.id = d.id_note)
            WHERE d.estado = 1 AND d.id_user = '$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/detalleOrder/orders/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT d.id, c.contractors, p.problem, l.location, e.equipo, n.notas,estado, id_order
    FROM detalle_orders d
    INNER JOIN orders o ON (o.id = d.id_order)
    INNER JOIN contractors c ON (c.id = d.contractors)
    INNER JOIN problems p ON (p.id = d.problems)
    INNER JOIN location l ON (l.id = d.location)
    INNER JOIN equipo e ON (e.id = d.equipo)
    INNER JOIN notas n ON (n.id = d.id_note)
    WHERE d.estado = 1 AND d.id_order = '$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->delete('/api/detalleOrder/delete/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "DELETE FROM detalle_orders WHERE id = '$id'";

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Detail successfully removed',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});
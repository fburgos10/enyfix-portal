<?php

$app->get('/api/compania/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT * FROM compania WHERE negocio = '$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->post('/api/compania/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $nombre = $request->getParam('nombre');
    $telefono = $request->getParam('telefono');
    $direccion = $request->getParam('direccion');
    $rnc = $request->getParam('rnc');
    $email = $request->getParam('email');
    $id_negocio = $request->getParam('id_negocio');
    $fecha =  date("Y-m-d H:i:s a"); 

    $sql = "INSERT INTO compania (nombre, telefono, direccion, email, negocio)
        VALUES (:nombre, :telefono, :direccion, :email, :negocio)";

    try{
        
        $db = new db();
        $db = $db->connect();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':nombre', $data['nombre']);
        $stmt->bindParam(':telefono', $data['telefono']);
        $stmt->bindParam(':direccion', $data['direccion']);
        $stmt->bindParam(':email', $data['email']);
        $stmt->bindParam(':negocio', $data['negocio']);
        $stmt->execute();

        $message = [
          'api' => 'Company successfully add',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->delete('/api/compania/delete/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "DELETE FROM compania WHERE id = '$id'";

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Company successfully removed',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});
<?php
// Consultar todos las factura
$app->get('/api/factura', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $sql = "SELECT * FROM ordenes WHERE id = (SELECT MAX(id) FROM ordenes)";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Consulta de una factura en especifico
$app->get('/api/factura/{id}', function ($request, $response, $args) {
    
    date_default_timezone_set('America/Santo_Domingo');
    $id = $args['id'];
    $sql = "SELECT d.id_cart ,o.id as idOrden, a.articulo ,o.fecha as fecha, idCliente, cantidad, d.idArticulo, precioUnitario, precioTotal, status, tipoServicio FROM ordenes o 
        INNER JOIN detalle_venta d ON (o.id = d.idOrden)
        INNER JOIN articulos a ON (a.id = d.idArticulo)
        WHERE o.id = (SELECT MAX(id) FROM ordenes WHERE id_user = '$id') AND status = 2";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

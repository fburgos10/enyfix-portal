<?php
// Consultar todos los clientes
$app->get('/api/direcciones', function ($request, $response, $args) {

    $sql = "SELECT * FROM direcciones";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/direcciones/{id}/{tipo}', function ($request, $response, $args) {
    
    $id = $args['id'];
    $tipo = $args['tipo'];

    $sql = "SELECT * FROM direcciones WHERE id_usuario = '$id' AND tipo= '$tipo' ";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// A単adir un nuevo cliente
$app->post('/api/direcciones/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $direccion = $request->getParam('direccion');
    $ciudad = $request->getParam('ciudad');
    $edificio = $request->getParam('edificio');
    $type = $request->getParam('type');
    $lat = $request->getParam('lat');
    $lon = $request->getParam('lon');
    $id_usuario = $request->getParam('id_usuario');
    $fecha =  date("Y-m-d H:i:s");
    $adres = utf8_decode($direccion);

    $sql ="INSERT INTO direcciones (id_usuario, tipo, ciudad, cod_postal, calle, lugar_detalle, lat, lng) VALUES
    ('$id_usuario','$type','$ciudad','00000','$adres','$adres','$lat','$lon')";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Cliente registrado con exitos',
          'Company' => 'Enyfix',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Editar un cliente
/*$app->put('/api/direcciones/update/{id}', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $id = $args['id'];

    $direccion = $request->getParam('direccion');
    $ciudad = $request->getParam('ciudad');
    $edificio = $request->getParam('edificio');
    $type = $request->getParam('type');
    $lat = $request->getParam('lat');
    $lon = $request->getParam('lon');
    $id_usuario = $request->getParam('id_usuario');
    $fecha =  date("Y-m-d H:i:s");
    $adres = utf8_decode($direccion);

    $sql = "UPDATE direcciones SET 
              nombre = '$nombre',
              apellido = '$apellido',
              direccion = '$direccion',
              cedula  = '$cedula',
              telefono  = '$telefono',
              email = '$email',
              foto = '$foto',
              estado = '$estado',
              usuario = '$usuario',
              cedula = '$cedula'
              WHERE id_cliente = ".$id;

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Cliente actualizado con exitos',
          'Company' => 'Moviwash',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});*/

//Cambiar contrase単a del cliente
$app->delete('/api/direcciones/delete/{id}', function ($request, $response, $args) {

    $id = $args['id'];

    $sql = "DELETE FROM direcciones WHERE id =".$id;

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Direccion eliminada con exitos',
          'Company' => 'Enyfix',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


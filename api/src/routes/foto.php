<?php
// Consultar todos los clientes
$app->put('/api/photo/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $id = $request->getParam('id');
    $foto = $request->getParam('foto');
    $fecha =  date("Y-m-d H:i:s a"); 
    
    $sql ="UPDATE cliente SET foto=:foto WHERE id=:id";

    try{

        $db = new db();
        $db = $db->connect();
        $stmt = $db->prepare($sql);

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':foto',  $foto);
        $stmt->execute();

        $message = [
          'api' => 'Photo successfully registered',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

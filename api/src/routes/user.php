<?php
// Consultar todos los clientes
$app->get('/api/cliente', function ($request, $response, $args) {

    $sql = "SELECT * FROM cliente";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Añadir un nuevo cliente
$app->post('/api/cliente/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $usuario = $request->getParam('usuario');
    $nombre = $request->getParam('nombre');
    $apellido = $request->getParam('apellido');
    $direccion = $request->getParam('direccion');
    $cedula = $request->getParam('cedula');
    $telefono = $request->getParam('telefono');
    $email = $request->getParam('email');
    $foto = $request->getParam('foto');
    $pass = $request->getParam('pass');
    $cedula = $request->getParam('cedula');
    $fecha =  date("Y-m-d H:i:s");
    $adres = utf8_decode($direccion);
    $hash = password_hash($pass, PASSWORD_DEFAULT);

    $sql ="INSERT INTO cliente (nombre, apellido, direccion, cedula, telefono, email, foto, pass, fecha_creacion,usuario) VALUES
    ('$nombre','$apellido','$adres','$cedula','$telefono','$email','$foto','$hash','$fecha','$usuario')";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Cliente registrado con exitos',
          'Company' => 'Moviwash',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Editar un cliente
$app->put('/api/cliente/update/{id}', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $id = $args['id'];
    $usuario = $request->getParam('usuario');
    $nombre = $request->getParam('nombre');
    $apellido = $request->getParam('apellido');
    $direccion = $request->getParam('direccion');
    $cedula = $request->getParam('cedula');
    $telefono = $request->getParam('telefono');
    $email = $request->getParam('email');
    $foto = $request->getParam('foto');
    $pass = $request->getParam('pass');
    $fecha =  date("Y-m-d H:i:s");
    $adres = utf8_decode($direccion);
    $cedula = $request->getParam('cedula');
    $hash = password_hash($pass, PASSWORD_DEFAULT);

    $sql = "UPDATE cliente SET 
              nombre = '$nombre',
              apellido = '$apellido',
              direccion = '$direccion',
              cedula  = '$cedula',
              telefono  = '$telefono',
              email = '$email',
              foto = '$foto',
              estado = '$estado',
              usuario = '$usuario',
              cedula = '$cedula'
              WHERE id_cliente = ".$id;

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Cliente actualizado con exitos',
          'Company' => 'Moviwash',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

//Cambiar contraseña del cliente
$app->put('/api/cliente/pass/{id}', function ($request, $response, $args) {

    //$id = $request->getAttribute('id');
    $id = $args['id'];
    $pass = $request->getParam('pass');
    $hash = password_hash($pass, PASSWORD_DEFAULT);

    $sql = "UPDATE cliente SET 
              pass = '$pass'
              WHERE id_cliente = ".$id;

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Contraseña actulizada con exitos',
          'Company' => 'Moviwash',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


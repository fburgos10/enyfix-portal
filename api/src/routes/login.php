<?php
// Iniciar session
$app->post('/api/login', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $email = $request->getParam('email');
    $password = $request->getParam('password');
    $fecha = date('Y-m-d H:i:s');

    // Get DB Object
    $db = new db();
    // Connect
    $db = $db->connect();

    $sql = "SELECT * FROM cliente  WHERE  usuario = '$email' OR email = '$email'";
    $query = $db->query($sql);
    $result_row = $query->fetch(PDO::FETCH_ASSOC);
    
    //Contar la cantidad de registros
    $num_rows = $db->query("SELECT count(id) from cliente WHERE  usuario = '$email' OR email = '$email'")->rowCount();
    
    $clave =  $result_row['pass'];
    if ($num_rows == 1) {
        
        if (password_verify($password, $clave)) {
            $id = $result_row['id'];

            $consulta = $db->query("UPDATE cliente SET session_id ='2', last_login ='$fecha' WHERE id ='$id'");
            $db = null;
            $payload = json_encode($result_row);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

        } else {
          echo  $errors = "1";
        }
    } else {
       echo $errors = "2";
    }
});

// Cerrar session
$app->put('/api/logout/{id}', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $id = $request->getAttribute('id');

    //$sql = "UPDATE cliente SET session_id = 1, token ='' WHERE id = '$id'";
    $sql = "UPDATE cliente SET
                session_id  = 1,
                token   = ''
            WHERE id = $id";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->execute();

        echo '{"notice": {"text": "Customer Updated"}';

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

<?php

$app->get('/api/info/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT * FROM cliente WHERE id = '$id'";

    try{

        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});
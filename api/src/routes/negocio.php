<?php
// Consultar todos los clientes
$app->get('/api/store/{negocio}', function ($request, $response, $args) {

    $id = $args['negocio'];
    $sql = "SELECT s.* FROM sucursal s
            INNER JOIN negocio n ON (n.id = s.negocio)
            WHERE s.negocio =  '$id' ";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $store = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($store);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/store/sucursal/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT * FROM sucursal WHERE id = '$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $store = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($store);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->delete('/api/store/delete/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "DELETE FROM sucursal WHERE id = ".$id;

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Store successfully removed',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->post('/api/store/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $nombre = $request->getParam('nombre');
    $telefono = $request->getParam('telefono');
    $correo = $request->getParam('correo');
    $direccion = $request->getParam('direccion');
    $negocio = $request->getParam('negocio');
    $id_manager = $request->getParam('id_manager');
    $fecha =  date("Y-m-d H:i:s a"); 
    
    $sql ="INSERT INTO sucursal (nombre, telefono, correo, direccion, negocio, id_manager)
           VALUES ('$nombre','$telefono','$correo','$direccion','$negocio','$id_manager')";

    try{

        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Store successfully registered',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/equipol/{categoria}', function ($request, $response, $args) {

    $categoria = $args['categoria'];
    $sql = "SELECT * FROM equipo WHERE categoria = '$categoria'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $store = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($store);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/problemsi/{contrators}', function ($request, $response, $args) {

    $contrators = $args['contrators'];
    $sql = "SELECT * FROM problems WHERE id_contractors ='$contrators'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $store = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($store);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/problemti/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT * FROM problems WHERE id = '$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $store = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($store);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->post('/api/orde/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $LocationStoreID = $request->getParam('LocationStoreID');
    $ProblemIssueId = $request->getParam('ProblemIssueId');
    $compaySel = $request->getParam('compaySel');
    $notas = $request->getParam('notas');
    $user = $request->getParam('user');
    $fecha =  date("Y-m-d H:i:s");


    $sql ="INSERT INTO orderes (sucursal, issue, company, fecha, nota, user) VALUES ('$LocationStoreID','$ProblemIssueId','$compaySel','$fecha','$notas','$user')";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Order create',
          'Company' => 'Enyfix',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/orde/open', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT s.nombre AS sucursal, o.id, s.direccion, CONCAT(c.nombre,' ',c.apellido) AS nombre, s.foto FROM orderes o
INNER JOIN cliente c ON (c.id = o.user)
INNER JOIN sucursal s ON (s.id = o.sucursal)
INNER JOIN problems p ON (p.id = o.issue)
INNER JOIN compania co ON (co.id = o.company)";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $store = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($store);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});
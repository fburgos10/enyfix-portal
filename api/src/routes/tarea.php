<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

// Add Task
$app->get('/api/task/add/key=eyJ1c2VyIjoiZmVybmFuZG8iLCJwcyI6ImYxMDEyMDQyMSIsImFsZyI6IkhTMjU2In0', function(Request $request, Response $response){
    date_default_timezone_set('America/Santo_Domingo');
    $task_id = $request->getParam('task_id');
    $cliente = $request->getParam('cliente');
    $tipo = $request->getParam('tipo');
    $descripcion = $request->getParam('descripcion');
    //$conductor = $request->getParam('conductor');
    $direccion = $request->getParam('direccion');
    $lat = $request->getParam('lat');
    $long = $request->getParam('long');
    $hora_r = $request->getParam('hora_r'); //Hora de recogida
    $fecha_r = $request->getParam('fecha_r'); //Fecha de Recogida
    $fecha =  date("Y-m-d H:i:s");
    $adres = utf8_decode($direccion);

    $sql = "INSERT INTO tarea (task_id, cliente, tipo, descripcion, direccion, fecha, botones,lat,longi,fecha_r,hora_r) VALUES
    ('$task_id','$cliente','2','$descripcion','$adres','$fecha','Aceptada','$lat','$long','$fecha_r','$hora_r')";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        //echo json_encode($customer);
        echo '{"notice": {"text": "Task Add"}';
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
        header('Location: http://laundryappmovil.com/driverapp/apis/nueva_orden.php?task_id='.$task_id.'&lat='.$lat.'&long='.$long);

    }
});

$app->get('/api/task/{id}/key=eyJ1c2VyIjoiZmVybmFuZG8iLCJwcyI6ImYxMDEyMDQyMSIsImFsZyI6IkhTMjU2In0', function(Request $request, Response $response){
    $id = $request->getAttribute('id');

    $sql = "SELECT * FROM tarea WHERE task_id = $id";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($customer);
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/chat/key=eyJ1c2VyIjoiZmVybmFuZG8iLCJwcyI6ImYxMDEyMDQyMSIsImFsZyI6IkhTMjU2In0', function(Request $request, Response $response){
    date_default_timezone_set('America/Santo_Domingo');
    $titulo = 'conductor';
    $mensaje = $request->getParam('mensaje');
    $id_creador = $request->getParam('id_creador');
    $task = $request->getParam('task');
    $status = 1;
    $fecha =  date("Y-m-d H:i:s");

    $sqli = "INSERT INTO chat_mensajes (id_orden, mensaje, emisor, fecha) VALUES
    ('$task','$mensaje','$titulo','$fecha')";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sqli);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        //echo json_encode($customer);
        echo '{"notice": {"text": "Task Add"}';
    } catch(PDOException $e){
        ///echo '{"error": {"text": '.$e->getMessage().'}';
        //echo json_encode();
        
    }
});

$app->get('/api/chat-cons/key=eyJ1c2VyIjoiZmVybmFuZG8iLCJwcyI6ImYxMDEyMDQyMSIsImFsZyI6IkhTMjU2In0', function(Request $request, Response $response){

    $task = $request->getParam('task');

    $sqli = "SELECT * FROM chat_mensajes WHERE id_orden = $task";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sqli);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($customer);
        echo '{"notice": {"text": "Task Add"}';
    } catch(PDOException $e){
        echo json_encode();
    }
});

$app->get('/api/notify-chat', function(Request $request, Response $response){
    $task = $request->getParam('task');
    $cliente = $request->getParam('id_cliente');
    $id_chat = $request->getParam('id_chat');

    header('Location: http://laundryappmovil.com/driverapp/apis/notify-chat.php?task_id='.$task_id.'&cliente='.$cliente.'&id_chat='.$id_chat);
});


//Add Direcciones del cliente.
/*$app->get('/api/address/add', function(Request $request, Response $response){
    date_default_timezone_set('America/Santo_Domingo');
    $fecha =  date("Y-m-d H:i:s");

    echo $id_cliente = $request->getParam('id_cliente');
    echo $tipo = $request->getParam('tipo');
    echo $ciudad = $request->getParam('ciudad');
    echo $zip = $request->getParam('zip');
    echo $calle = $request->getParam('calle');
    echo $direccion = $request->getParam('direccion');
    echo $lat = $request->getParam('lat');
    echo $long = $request->getParam('long');

    $cons = "SELECT * FROM direcciones WHERE id_usuario = '$id_cliente' AND tipo ='$tipo'";
    

    try{
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $result_of_login_check->fetch(PDO::FETCH_OBJ);
        $customer2 = $stmt->fetch(PDO::FETCH_OBJ);
        $result_of_login_check = $db->query($cons);
    if ($result_of_login_check->num_rows > 0) {

        $sql ="UPDATE direcciones SET id_usuario = '$id_cliente', tipo = '$tipo', ciudad = '$ciudad', cod_postal = '$zip', calle = '$calle', lugar_detalle = '$direccion', lat = '$lat', lng= '$long' WHERE id_usuario = '$id_cliente' AND tipo = '$tipo'";
    }else{
        $sql = "INSERT INTO direcciones (id_usuario, tipo, ciudad, cod_postal, calle, lugar_detalle, lat,lng) VALUES
    ('$id_cliente','$tipo','$ciudad','$zip','$calle','$direccion','$lat','$long')";
    }
        $db = null;
        echo '{"notice": {"text": "Task Add"}';
    } catch(PDOException $e){
        //echo '{"error": {"text": '.$e->getMessage().'}';
        echo '{"notice": {"text": "Address Add"}';

    }
});*/

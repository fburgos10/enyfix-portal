<?php
// Consultar todos los clientes
$app->get('/api/orden', function ($request, $response, $args) {

    $sql = "SELECT * FROM ordenes";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Consultar todos los clientes
$app->get('/api/orden/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT * , 
CASE WHEN estado = 1 THEN 'Proceso de recogida' 
WHEN estado = 2 THEN 'Camino a su destino' 
WHEN estado = 3 THEN 'Proceso de lavado' 
WHEN estado = 4 THEN 'Proceso de entrega' 
WHEN estado = 5 THEN 'Camino a su destino' 
WHEN estado = 6 THEN 'Orden entregada' 
ELSE 'Orden declinada' END AS estados 
FROM ordenes WHERE id_user = '$id' ORDER BY id DESC LIMIT 10";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

// Consultar todos los clientes
$app->get('/api/orden/consulta/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT d.id_cart ,o.id as idOrden, a.articulo ,o.fecha as fecha, idCliente, cantidad, d.idArticulo, precioUnitario, precioTotal, status, tipoServicio,id_tanda_r,id_tanda_e,fecha_recogida,fecha_entrega,est.nombre as estado
FROM ordenes o 
INNER JOIN detalle_venta d ON (o.id = d.idOrden)
INNER JOIN articulos a ON (a.id = d.idArticulo)
INNER JOIN ordenes_est est ON (o.estado = est.id)
WHERE o.id = '$id' AND status = 2";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


// Consultar todos los clientes
$app->get('/api/orden/timeline/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT * FROM proceso WHERE task_id = '$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});




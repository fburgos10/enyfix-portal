<?php
// Consultar todos los articulos del carrito
$app->get('/api/carrito', function ($request, $response, $args) {

    $sql = "SELECT * FROM detalle_venta d
            INNER JOIN cliente c ON c.id_cliente = d.idCliente
            INNER JOIN articulos a ON a.id = d.idArticulo
            INNER JOIN tipos_serv t ON t.id = d.tipoServicio";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/carrito/{id}', function ($request, $response, $args) {
    
    $id = $args['id'];
    $sql = "SELECT * FROM detalle_venta d
            INNER JOIN cliente c ON c.id_cliente = d.idCliente
            INNER JOIN articulos a ON a.id = d.idArticulo
            INNER JOIN tipos_serv t ON t.id = d.tipoServicio
            WHERE d.idCliente = '$id' AND status = 1";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($customers);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

//Cambiar contrase単a del cliente
$app->delete('/api/carrito/delete/{id}', function ($request, $response, $args) {

    //$id = $request->getAttribute('id');
    $id = $args['id'];
    $sql = "DELETE FROM detalle_venta WHERE id_cart = '$id'";

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Articulo Eliminado con exitos',
          'Company' => 'Moviwash',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

//Limpiar tabla
$app->delete('/api/carrito/limpiar/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "DELETE FROM detalle_venta WHERE idCliente = '$id' AND cantidad = 0 AND precioTotal = 0 AND status = 1";

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Articulo Eliminado con exitos',
          'Company' => 'Moviwash',
          'tiempo de ejecucion' => time(),
          'Fecha de ejecucion' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


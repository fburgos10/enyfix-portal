<?php

$app->get('/api/sucursales/{id}/{id_sucursal}', function ($request, $response, $args) {

    $id = $args['id'];
    $id_sucursal = $args['id_sucursal'];

    if ($id_sucursal == 0) {
        $sql = "SELECT * FROM sucursal WHERE negocio = '$id'";
    } else {
       $sql = "SELECT * FROM sucursal WHERE negocio = '$id' AND id = '$id_sucursal'";
    }

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


$app->get('/api/sucursales', function ($request, $response, $args) {

        $sql = "SELECT * FROM sucursal";
    

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


//Add new sucursal
$app->post('/api/sucursal/add/', function(Request $request, Response $response){
    $sucursal_name = $request->getParam('nombre');
    $phone = $request->getParam('phone');
    $email = $request->getParam('email');
    $address = $request->getParam('address');
    $foto = $request->getParam('foto');
    $img = $_FILES;
    var_dump($img);
    exit;

    $sql = "INSERT INTO sucursal (nombre,telefono,correo,direccion,foto) VALUES
    (:nombre,:phone,:email,:address,:foto)";

    // try{
    //     // Get DB Object
    //     $db = new db();
    //     // Connect
    //     $db = $db->connect();

    //     $stmt = $db->prepare($sql);

    //     $stmt->bindParam(':nombre', $sucursal_name);
    //     $stmt->bindParam(':phone',      $phone);
    //     $stmt->bindParam(':email',      $email);
    //     $stmt->bindParam(':address',    $address);
    //     $stmt->bindParam(':foto',    $foto);

    //     $stmt->execute();

    //     echo '{"notice": {"text": "Customer Added"}';

    // } catch(PDOException $e){
    //     echo '{"error": {"text": '.$e->getMessage().'}';
    // }
});


$app->get('/api/sucursalesti/{id}', function ($request, $response, $args) {

    $id = $args['id'];
   $sql = "SELECT * FROM sucursal WHERE id = '$id'";


    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});


$app->get('/api/categoria/{id}/{id_sucursal}', function ($request, $response, $args) {

    $id = $args['id'];
    $id_sucursal = $args['id_sucursal'];

    if ($id_sucursal == 0) {
        $sql = "SELECT c.* FROM categoria c
INNER JOIN equipo e ON c.id = e.categoria
GROUP BY c.categoria";
    } else {
       $sql = "SELECT * FROM categoria WHERE id_negocio = '$id' AND id_sucursal = '$id_sucursal'";
    }

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $memos = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($memos);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

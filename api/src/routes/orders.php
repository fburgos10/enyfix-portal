<?php
// Consultar todos los clientes
$app->get('/api/orders', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT o.id,s.nombre AS sucursal, CONCAT(c.nombre,' ',c.apellido) AS nombreCliente, co.nombre AS compania, t.nombre AS tecnico,
    o.fecha_creacion, o.fecha_vencimiento, 
    CONCAT(cli.nombre,' ',cli.apellido) AS representanteNegocio,s.direccion AS direccionNegocio,
    s.telefono AS telefonoNegocio, co.telefono AS telefonoCompany, co.direccion AS direccionCompany, co.email AS emailCompany,
    CASE
    WHEN o.prioridad = 1 THEN 'Low'
    WHEN o.prioridad = 2 THEN 'Medium'
    ELSE 'High'
    END AS prioridades,
    CASE
    WHEN o.estatus = 1 THEN 'Order created'
    WHEN o.estatus = 2 THEN 'Order accepted'
    WHEN o.estatus = 3 THEN 'Order in process'
    ELSE 'Order finished'
    END AS estatus, s.foto
    FROM orders o
    INNER JOIN sucursal s ON (s.id = o.id_sucursal)
    INNER JOIN cliente cli ON (cli.id = s.id_manager)
    INNER JOIN cliente c ON (c.id = o.id_user)
    INNER JOIN compania co ON (co.id = o.id_compania)
    LEFT JOIN tecnico t ON (t.id = o.id_tecnico)";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $orders = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($orders);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/orders/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT s.nombre AS sucursal, CONCAT(c.nombre,' ',c.apellido) AS nombreCliente, co.nombre AS compania, t.nombre AS tecnico,
    o.fecha_creacion, o.fecha_vencimiento, 
    CONCAT(cli.nombre,' ',cli.apellido) AS representanteNegocio,s.direccion AS direccionNegocio,
    s.telefono AS telefonoNegocio, co.telefono AS telefonoCompany, co.direccion AS direccionCompany, co.email AS emailCompany,
    CASE
    WHEN o.prioridad = 1 THEN 'Low'
    WHEN o.prioridad = 2 THEN 'Medium'
    ELSE 'High'
    END AS prioridades,
    CASE
    WHEN o.estatus = 1 THEN 'Order created'
    WHEN o.estatus = 2 THEN 'Order accepted'
    WHEN o.estatus = 3 THEN 'Order in process'
    ELSE 'Order finished'
    END AS estatus
    FROM orders o
    INNER JOIN sucursal s ON (s.id = o.id_sucursal)
    INNER JOIN cliente cli ON (cli.id = s.id_manager)
    INNER JOIN cliente c ON (c.id = o.id_user)
    INNER JOIN compania co ON (co.id = o.id_compania)
    LEFT JOIN tecnico t ON (t.id = o.id_tecnico)
    WHERE o.id ='$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $orders = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($orders);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/orders/client/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT s.nombre AS sucursal, CONCAT(c.nombre,' ',c.apellido) AS nombreCliente, co.nombre AS compania, t.nombre AS tecnico,
    o.fecha_creacion, o.fecha_vencimiento,
    CASE
    WHEN o.prioridad = 1 THEN 'Low'
    WHEN o.prioridad = 2 THEN 'Medium'
    ELSE 'High'
    END AS prioridades,
    CASE
    WHEN o.estatus = 1 THEN 'Order created'
    WHEN o.estatus = 2 THEN 'Order accepted'
    WHEN o.estatus = 3 THEN 'Order in process'
    ELSE 'Order finished'
    END AS estatus
    FROM orders o
    INNER JOIN sucursal s ON (s.id = o.id_sucursal)
    INNER JOIN cliente c ON (c.id = o.id_user)
    INNER JOIN compania co ON (co.id = o.id_compania)
    LEFT JOIN tecnico t ON (t.id = o.id_tecnico)
    WHERE c.id ='$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $orders = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($orders);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});




$app->get('/api/orders/sucursal/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT s.nombre AS sucursal, CONCAT(c.nombre,' ',c.apellido) AS nombreCliente, co.nombre AS compania, t.nombre AS tecnico,
    o.fecha_creacion, o.fecha_vencimiento, 
    CONCAT(cli.nombre,' ',cli.apellido) AS representanteNegocio,s.direccion AS direccionNegocio,
    s.telefono AS telefonoNegocio, co.telefono AS telefonoCompany, co.direccion AS direccionCompany, co.email AS emailCompany,
    CASE
    WHEN o.prioridad = 1 THEN 'Low'
    WHEN o.prioridad = 2 THEN 'Medium'
    ELSE 'High'
    END AS prioridades,
    CASE
    WHEN o.estatus = 1 THEN 'Order created'
    WHEN o.estatus = 2 THEN 'Order accepted'
    WHEN o.estatus = 3 THEN 'Order in process'
    ELSE 'Order finished'
    END AS estatus
    FROM orders o
    INNER JOIN sucursal s ON (s.id = o.id_sucursal)
    INNER JOIN cliente cli ON (cli.id = s.id_manager)
    INNER JOIN cliente c ON (c.id = o.id_user)
    INNER JOIN compania co ON (co.id = o.id_compania)
    INNER JOIN tecnico t ON (t.id = o.id_tecnico)
    WHERE o.id_sucursal = '$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $orders = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($orders);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->delete('/api/orders/delete/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "DELETE FROM orders WHERE id = ".$id;

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Orders successfully removed',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->post('/api/orders/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $id_sucursal = $request->getParam('id_sucursal');
    $id_user = $request->getParam('id_user');
    $id_compania = $request->getParam('id_compania');
    $fecha_vencimiento = $request->getParam('fecha_vencimiento');
    $prioridad = $request->getParam('prioridad');
    $fecha =  date("Y-m-d H:i:s a"); 
    
    $sql ="INSERT INTO orders (id_sucursal, id_user, id_compania, id_tecnico, fecha_creacion,fecha_vencimiento,prioridad) 
           VALUES ('$id_sucursal','$id_user','$id_compania','$fecha','$fecha_vencimiento','$prioridad')";

    try{

        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Orders successfully registered',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});
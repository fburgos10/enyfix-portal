<?php
// Consultar todos los clientes
$app->get('/api/inventory/{negocio}', function ($request, $response, $args) {

    $id = $args['negocio'];
    $sql = "SELECT e.* FROM equipo e
            INNER JOIN sucursal s ON (s.id = e.id_sucursal)
            INNER JOIN negocio n ON (n.id = s.negocio)
            WHERE n.id ='$id' ";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $inventory = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($inventory);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/inventory/sucursal/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "SELECT * FROM equipo WHERE id_sucursal = '$id'";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $inventory = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $payload = json_encode($inventory);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->delete('/api/inventory/delete/{id}', function ($request, $response, $args) {

    $id = $args['id'];
    $sql = "DELETE FROM equipo WHERE id = ".$id;

    try{
        // Get DB Object
        $db = new db();
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Appliances successfully removed',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        
    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->post('/api/inventory/add', function ($request, $response, $args) {

    date_default_timezone_set('America/Santo_Domingo');
    $equipo = $request->getParam('equipo');
    $color = $request->getParam('color');
    $marca = $request->getParam('marca');
    $modelo = $request->getParam('modelo');
    $ano = $request->getParam('ano');
    $garantia = $request->getParam('garantia');
    $fecha_compra = $request->getParam('fecha_compra');
    $recomendacion = $request->getParam('recomendacion');
    $manual = $request->getParam('manual');
    $id_sucursal = $request->getParam('id_sucursal');
    $categoria = $request->getParam('categoria');
    $fecha =  date("Y-m-d H:i:s a"); 
    
    $sql ="INSERT INTO equipo (equipo, color, marca, modelo, ano, garantia, fecha_compra, recomendacion, manual, id_sucursal, categoria)
        VALUES ('$equipo','$color','$marca','$modelo','$ano','$garantia','$fecha_compra','$recomendacion','$manual','$id_sucursal','$categoria');";

    try{

        $db = new db();
        // Connect
        $db = $db->connect();
        $stmt = $db->query($sql);
        $customer = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;

        $message = [
          'api' => 'Appliances successfully registered',
          'Company' => 'Enyfix',
          'Execution time' => time(),
          'Execution date' => date('Y-m-d'),
        ];

        $payload = json_encode($message);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});




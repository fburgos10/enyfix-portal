$(document).ready(function(){
    const infoHomeDisplay = document.querySelector('#fixer_table');
    let html = '';
    $.ajax({
        url: "http://52.14.84.187/api-enyfix/public/api/compania/1",
        type: "GET",
        success: function(data){
        console.log(data);
        data.forEach(item =>{
            html +=`<tr>
            <td>${item.nombre}</td>
            <td>${item.rnc}</td>
            <td>${item.direccion}</td>
            <td>${item.telefono}</td>
            <td>${item.email}</td>
            <td>
              <button class="btn btn-success waves-effect md-trigger" onclick="asignation(3, ${item.id})" style="padding: 5px;" data-toggle="modal" data-placement="top" data-target="#detailStoreModal" data-original-title="All Info Store">
                <span class="material-icons">visibility</span>
              </button>
              <button class="btn btn-warning" style="padding: 5px;" onclick="asignation(1, ${item.id})" data-toggle="modal" data-target="#storeModal" data-placement="top" data-original-title="Edit Store">
                <span class="material-icons">edit</span>
              </button>
              <button class="btn btn-danger" style="padding: 5px;" onclick="asignation(2, ${item.id})" data-placement="top" data-original-title="Delete Store">
                <span class="material-icons">delete</span>
              </button>
            </td>
          </tr>`;
        });
        infoHomeDisplay.innerHTML = html;
        }
    });
});
var action;
var logo;
var sucursal_id;
$(document).ready(function(){
    $('#leurisPapi').val('leuris la para');
    const infoHomeDisplay = document.querySelector('#sucursales');
    let html = '';
    $.ajax({
        url: "http://52.14.84.187/api-enyfix/public/api/sucursales",
        type: "GET",
        success: function(data){
        console.log(data);
        data.forEach(item =>{
            html +=`<tr>
            <td>
              <img src="http://52.14.84.187/${item.foto.replace("img", "image")}" alt="store img" class="img-fluid" style="width: 50px;height: 50px;">
            </td>
            <td>${item.nombre}</td>
            <td>Hector Matos</td>
            <td>${item.direccion}</td>
            <td>${item.telefono}</td>
            <td align="center">16</td>
            <td>
              <button class="btn btn-success waves-effect md-trigger" onclick="asignation(3, ${item.id})" style="padding: 5px;" data-toggle="modal" data-placement="top" data-target="#detailStoreModal" data-original-title="All Info Store">
                <span class="material-icons">visibility</span>
              </button>
              <button class="btn btn-warning" style="padding: 5px;" onclick="asignation(1, ${item.id})" data-toggle="modal" data-target="#storeModal" data-placement="top" data-original-title="Edit Store">
                <span class="material-icons">edit</span>
              </button>
              <button class="btn btn-danger" style="padding: 5px;" onclick="asignation(2, ${item.id})" data-placement="top" data-original-title="Delete Store">
                <span class="material-icons">delete</span>
              </button>
            </td>
          </tr>`;
        });
        infoHomeDisplay.innerHTML = html;
        }
    });
});

function asignation(val, id)
{
  action = val;
  sucursal_id = id;
  if(action == 1)
  {
    $.ajax({
        type: "GET",
        url: "http://52.14.84.187/api-enyfix/public/api/sucursalesti/" + id,
        success: function(data){
          $('#name').val(data[0]['nombre']);
          $('#phone').val(data[0]['telefono']);
          $('#email').val(data[0]['correo']);
          $('#address').val(data[0]['direccion']);
          logo = data[0]['foto'];
        }
    });
  }
  else if(action == 2)
  {
    managestore();
  }
  else if(action == 3)
  {
    $.ajax({
      type: "GET",
      url: "http://52.14.84.187/api-enyfix/public/api/sucursalesif/" + id,
      success: function(data){
        ver(data[0]['nombre'],
        data[0]['telefono'],
        data[0]['correo'],
        data[0]['direccion'],
        data[0]['foto'],
        data[0]['negocio'],
        data[0]['manager']);
      }
    });
  }
}

function ver(pnombre, telefono, correo, direccion, foto, pnegocio, pmanager)
{
  var nombre = Elements("m_Nombre");
  var info = Elements("m_Info");
  var negocio = Elements("m_Negocio");
  var manager = Elements("m_Manager");
  nombre.textContent = pnombre;
  info.textContent = direccion + " / " + correo + " " + telefono;
  negocio.textContent = pnegocio;
  manager.textContent = pmanager;
}

function managestore()
{
  console.log(action);
  var formD = new FormData(Elements('new_Sucursal'));
  console.log(formD);
  var count = 0;
  var dt = new Array();
  formD.forEach(i => {
      dt[count] = i;
      count++;
  });
  var formData = new FormData();
  formData.append('name', dt[0]);
  formData.append('phone', dt[1]);
  formData.append('email', dt[2]);
  formData.append('address', dt[3]);
  formData.append('foto', dt[4]);
  formData.append('foto_vieja', logo);
  var id = localStorage.getItem('id');
  console.log(id);

  formData.forEach(o => {
    console.log(o);
  });
  if(action == 0)
  {
    $.ajax({
        type: "POST",
        url: "http://52.14.84.187/api-enyfix/public/api/sucursal/add/" + id,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function(xhr){
          console.log('lo toy enviando');
        },
        success: function(response){
          console.log(response);
          if(response == '0')
          {
            alert('Sucursal Added.');
          }
          else if(response == '1')
          {
            alert('Sorry, the new sucursal could not be created correctly.');
          }
        }
      });
  }
  else if(action == 1)
  {
    $.ajax({
      type: "POST",
      url: "http://52.14.84.187/api-enyfix/public/api/sucursal/update/" + sucursal_id + "/" + id,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      beforeSend: function(xhr){
        console.log('lo toy enviando');
      },
      success: function(response){
        console.log(response);
        if(response == '0')
        {
          alert('Sucursal Updated successfully.');
        }
        else if(response == '1')
        {
          alert('Sorry, the new sucursal could not be updated correctly. Please try again later');
        }
      }
    });
  }
  else if(action == 2)
  {
    if(confirm('Are you sure you want to delete this sucursal?'))
    {
      $.ajax({
        type: "PUT",
        url: "http://52.14.84.187/api-enyfix/public/api/sucursal/delete/" + sucursal_id,
        beforeSend: function(xhr){
          console.log('lo toy enviando');
        },
        success: function(response){
          console.log(response);
          if(response == '0')
          {
            alert('Sucursal deleted successfully.');
          }
          else if(response == '1')
          {
            alert('Sorry, the new sucursal could not be deleted correctly. Please try again later');
          }
        }
      });
    }
  }
}


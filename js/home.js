/*const bSubmit = document.querySelector('#bSubmit');

bSubmit.addEventListener('click', () =>{
// Validar campos
const nombre = document.querySelector('#nombre').value;
const rating = document.querySelector('#rating').value;

  if (nombre.trim() === '' || rating.trim() === '') return false;
  // Mandar solicitud POST a /new
  fetch('/new',{
    method : 'POST',
    headers: {'Content-type': 'application/json'},
    body : JSON.stringify({nombre: nombre, rating: rating})
  })
  .then(res => res.text())
  .then(data =>{
     alert(data);
     loadOrders(); //Cargar ordenes
   });
});*/

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

loadOrders(); //Cargar ordenes
OrderHistory(); //Cargar el historial de las ordenes por clientes

function loadOrders(){

 fetch('https://laundryappmovil.com/api-enyfix/public/api/orders', {method: 'GET'})
 .then(res => res.json())
 .then(data =>{
   const infoHomeDisplay = document.querySelector('#infoHomeDisplay');
   let html = '';
   console.log(data);
   data.forEach(item =>{
     html +=`<tr>
     <td>
     <img src="${item.foto}" alt="store img" class="img-fluid" style="width: 50px;height: 50px;">
     </td>
     <td>${item.sucursal}</td>
     <td>${item.nombreCliente}</td>
     <td>${item.telefonoNegocio}</td>
     <td>${item.direccionNegocio}</td>
     <td>
     <span class="label label-warning">${item.estatus}</span>
     </td>
     <td>#000${item.id}</td>
     </tr>`;
   });
   infoHomeDisplay.innerHTML = html;
 });
}

function OrderHistory()
{
  var id_com = getParameterByName('sucursal'); // ID de la compañia
  var id_customer = getParameterByName('id'); // ID de la orden
  var negocio = getParameterByName('negocio'); // ID de la orden
  const infoHomeDisplay = document.querySelector('#orderHistory');
  let html = '';
  $.ajax({
    url: "http://52.14.84.187/api-enyfix/public/api/orders/client/" + id_customer,
    type: "GET",
    success: function(data){
      console.log(data);
      data.forEach(item =>{
        html +=`<tr>
        <td>${item.sucursal}</td>
        <td>${item.compania}</td>
        <td>${item.fecha_creacion}</td>
        <td>${item.fecha_vencimiento}</td>
        <td>
        <span class="label label-warning">${item.estatus}</span>
        </td>
        </tr>`;
      });
      infoHomeDisplay.innerHTML = html;
    }
  });
}


// Como hacerlo con Ajax
/*function loadMovieAjax(){
  $.ajax({
   type: "GET",
   dataType: "json",
   url: "/get-peliculas",
   success: function(data){
     var html = "";
     $.each(data.peliculas, function(i, item) {
       html +=`<div>`+item.nombre+` <img src="img/star.png" width="16" /> `+item.rating+`</div>`;
       $('#peliculasContainerAJAX').html(html);
     });
   }
 });
}*/